/*!
*\file writing.h
*
*\brief Bibliothèque pour pouvoir écrire une image à partir d'une structure "image"
*
*\version 1.0
*\date 26/12/2020
*\author Antoine-Valentin CHARPENTIER & Bastien IVSAK
*/


#ifndef WRITING_H
#define WRITING_H

#include "utils.h"

/*!
*\brief Procédure qui permet de réaliser une copie de l'entête de l'image d'origine au format PPM ou PGM dans un autre fichier
*\param file : Fichier qui va recevoir l'entête du fichier d'origine
*\param img : Structure qui contient les informations sur l'image d'origine
*\param url : Le chemin d'accès à l'image d'origine dont on cherche à copier son entête
*/
void rewrite_header(FILE* file, image img, char* url);
/*!
*\brief Procédure qui écrit l'ensemble des pixels dans un fichier à partir de sa structure
*\param file : Fichier dans lequel on va écrire les pixels
*\param img : Structure qui contient les pixels de l'image
*/
void write_pixels(FILE* file, image img);
/*!
*\brief Fonction qui permet d'écrire une image dans son intégralité à partir de sa structure
*\param img : Structure qui contient les informations d'une image
*\param url : Chemin d'accès au fichier dans lequel on va générer l'image
*\return Un booléen qui prend la valeur "true" si l'écriture s'est déroulée sans problèmes, ou "false", s'il y a eu un problème
*/
boolean write_image(image img, char* url);


#endif
