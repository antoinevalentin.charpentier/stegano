/*!
*\file decode.h
*
*\brief Ensemble de fonctions nécessaire pour retrouver un message caché dans une image, selon les différentes améliorations proposées.
*
*\version 1.0
*\date 26/12/2020
*\author Antoine-Valentin CHARPENTIER & Bastien IVSAK
*/

#ifndef DECODE_H
#define DECODE_H

#include "utils.h"

/*!
*\brief Fonction qui permet de mettre en place/d'initialiser les prérequis des fonctions de décodage du message dans l'image et de proposer à l'utilisateur la sélection de la fonction de décodage à utiliser pour retrouver son message
*\return Un booléen qui prend la valeur "true" si le décodage s'est déroulé sans problèmes, ou "false", s'il y a eu un problème
*/
boolean start_decode();

/*!
*\brief Fonction qui permet de retrouver chaque bit du message dans les LSB des pixels de l'image, dans l'ordre de lecture. Le caractère "DEL" (caractère d'effacement utilisé à l'heure actuelle dans le système d'exploitation UNIX comme caractère de contrôle) marque la fin du décodage s'il est présent, s'il n'est pas présent, elle s'arrête lorsqu'il n'y a plus de place dans l'image pour cacher un octet.
*\param img : Structure "image" contenant l'ensemble des informations d'une image ainsi que le message caché.
*\param file : Pointeur vers le fichier qui va contenir le message caché dans l'image à la fin d'exécution de cette fonction.
*\return Un booléen qui prend la valeur "true" si le décodage s'est déroulé sans problèmes, ou "false", s'il y a eu un problème
*/
boolean decode_upgrade_1(image img, FILE* file);

/*!
*\brief Fonction qui permet de retrouver chaque bit du message dans les LSB des pixels de l'image, avec un parcours des pixels "aléatoire" généré par l'intermédiaire d'une chaîne de caractères (clé) saisie par l'utilisateur. Le caractère "DEL" (caractère d'effacement utilisé à l'heure actuelle dans le système d'exploitation UNIX comme caractère de contrôle) marque la fin du décodage s'il est présent, s'il n'est pas présent, elle s'arrête lorsqu'il n'y a plus de place dans l'image pour cacher un octet.
*\param img : Structure "image" contenant l'ensemble des informations d'une image ainsi que le message caché.
*\param file : Pointeur vers le fichier qui va contenir le message caché dans l'image à la fin d'exécution de cette fonction.
*\return Un booléen qui prend la valeur "true" si le décodage s'est déroulé sans problèmes, ou "false", s'il y a eu un problème
*/
boolean decode_upgrade_2(image img, FILE* file);

/*!
*\brief Fonction qui permet de retrouver chaque bit du message dans les LSB des pixels de l'image, caché par l'intermédiaire d'une matrice de contrôle (Hamming). Le message est terminé par le caractère "DEL" (caractère d'effacement utilisé à l'heure actuelle dans le système d'exploitation UNIX comme caractère de contrôle) s'il y a assez de place, sinon le message ne comporte pas de caractère de fin.
*\param img : Structure "image" contenant l'ensemble des informations d'une image ainsi que le message caché.
*\param file : Pointeur vers le fichier qui va contenir le message caché dans l'image à la fin d'exécution de cette fonction.
*\return Un booléen qui prend la valeur "true" si le décodage s'est déroulé sans problèmes, ou "false", s'il y a eu un problème
*/
boolean decode_upgrade_3(image img, FILE* file);

#endif
