#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "key.h"

void generate_srand_values(Key *key, unsigned int** srand_tab){
	/*FONCTION QUI GENERE UN ENSEMBLE DE VALEURS DE SRAND A PARTIR D'UNE CLE*/
	unsigned int value_srand = 0;
	int somme_produit = 0; //variable qui va permettre de savoir qu'elle opération on a fait sur la valeur précédente
	char c;

	*srand_tab = (unsigned int*)malloc(1*sizeof(unsigned int));//on intitialise un tableau dynamique d'une case,
	key->lenght = 0; //la longueur de la clé est actuellement de 0

	//on demande à l'utilisateur de saisir la clé
	printf("Veuillez saisir la cle (une chaine de caracteres) : ");
	//on récupère le premier caractère
	c = getchar();

	if(c == '\n'){
		c = getchar();
	}

	//on répète tant qu'il n'a pas fini d'écrire la clé
	while(c != '\n' && c != EOF){
		//on va ajouter un entier dans la liste, il faut alors préciser que la taille va augmenter
		(key->lenght)++;

		//on agrandit le tableau et on stocke la valeur qui va être utilisée dans le srand à l'intérieur de cette case
		*srand_tab = (unsigned int*)realloc(*srand_tab, (key->lenght)*sizeof(unsigned int));

		//on calcule la valeur que l'on va utiliser dans le srand pour faire un random pseudo aléatoire plus tard
		if(somme_produit == 0){
			value_srand = (value_srand + (c-31))%((unsigned int)pow(2,32));// le -31 permet de faire un décalage dans la table ascii pour ne pas avoir de valeur trop grande rapidement, ainsi le caractère espace représentera la valeur 1 au lieu de la valeur 32, et un modulo pow(2,32) pour ne pas dépasser la valeur maximale que prend la fonction srand en paramètre
		}else{
			value_srand = (value_srand * (c-31))%((unsigned int)pow(2,32));
		}
		somme_produit = !somme_produit;//la prochaine opération ne va pas être la même que celle d'avant

		//on rajoute la valeur saisie par l'utilisateur dans la liste des srand
		(*srand_tab)[(key->lenght)-1] = value_srand;

		//on récupère le caractère suivant
		c = getchar();
	}
}

void shuffle_bits_order(Key *key, unsigned int* srand_tab){
	/* FONCTION QUI FAIT UN RANDOM PSEUDO ALEATOIRE DANS LE TABLEAU*/
	unsigned int rnd, buffer; //rnd = variable random, et buffer = pour permuter deux valeurs dans le tableau

	//on alloue de la place pour le tableau et on le remplit : [0,1,2,3,...,(key->lenght)-1]
	key->order_bits = (unsigned int*)malloc((key->lenght)*sizeof(unsigned int));
	for(unsigned int k =0;k<key->lenght;k++){
		(key->order_bits)[k] = k;
	}

	//on mélange chaque case du tableau
	for(unsigned int i=0;i<key->lenght;i++){
		//on initialise le random, à partir de la valeur générée par la clé
		srand(srand_tab[i]);

		//on génère une valeur aléatoire comprise entre 0 et key->lenght-1
		rnd = rand()%(key->lenght);

		//on intervertit les cases du tableau key->order_bits d'indice i et rnd
		buffer = key->order_bits[rnd];
		key->order_bits[rnd] = key->order_bits[i];
		key->order_bits[i] = buffer;
	}
}

Key generate_bits_order(){
	/*FONCTION QUI GENERE UN ORDRE DE PARCOURS DES PIXELS EN FONCTION D'UNE CLE*/
	Key key;
	unsigned int *srand_tab;

	//on génère un ensemble de valeurs de srand à partir d'une clé saisie par l'utilisateur
	generate_srand_values(&key, &srand_tab);

	//on réalise un mélange de l'ordre de parcours des pixels à partir des valeurs de srand précédentes
	shuffle_bits_order(&key, srand_tab);

	//on libère l'espace alloué pour les valeurs de srand
	if(srand_tab){
		free(srand_tab);
	}

	return key;
}
