/*!
*\file encode.h
*
*\brief Ensemble de fonctions nécessaire pour dissimuler un message dans une image, selon les différentes améliorations proposées
*
*\version 1.0
*\date 26/12/2020
*\author Antoine-Valentin CHARPENTIER & Bastien IVSAK
*/

#ifndef ENCODE_H
#define ENCODE_H

#include "utils.h"

/*!
*\brief Fonction qui permet de mettre en place/d'initialiser les prérequis des fonctions d'encodage du message dans l'image et de proposer à l'utilisateur la sélection de la fonction d'encodage à utiliser pour dissimuler son message
*\return Un booléen qui prend la valeur "true" si l'encodage s'est déroulé sans problèmes, ou "false", s'il y a eu un problème
*/
boolean start_encode();

/*!
*\brief Fonction qui permet de cacher chaque bit du message dans les LSB des pixels de l'image, dans l'ordre de lecture. Le caractère "DEL" est dissimulé dans l'image à la fin du message, marque donc la fin du message s'il y a assez de place sinon il n'est pas présent à la fin du message.
*\param img : Pointeur vers une structure "image" contenant l'ensemble des informations d'une image, dont certains de ces pixels seront modifiés pour dissimuler le message
*\param file : Pointeur vers le fichier contenant le message que l'on cherche à cacher dans l'image
*\return Un booléen qui prend la valeur "true" si l'encodage s'est déroulé sans problèmes, ou "false", s'il y a eu un problème
*/
boolean encode_upgrade_1(image *img, FILE* file);

/*!
*\brief Fonction qui permet de cacher chaque bit du message dans les LSB des pixels de l'image, avec un parcours des pixels "aléatoire" généré par l'intermédiaire d'une chaîne de caractères (clé) saisie par l'utilisateur. Le caractère "DEL" est dissimulé dans l'image à la fin du message, marque donc la fin du message s'il y a assez de place sinon il n'est pas présent à la fin du message.
*\param img : Pointeur vers une structure "image" contenant l'ensemble des informations d'une image, dont certains de ces pixels seront modifiés pour dissimuler le message
*\param file : Pointeur vers le fichier contenant le message que l'on cherche à cacher dans l'image
*\return Un booléen qui prend la valeur "true" si l'encodage s'est déroulé sans problèmes, ou "false", s'il y a eu un problème
*/
boolean encode_upgrade_2(image *img, FILE* file);
/*!
*\brief Fonction qui permet de cacher le message dans l'image en réduisant drastiquement le nombre de modifications des LSB des pixels de l'image (donc augmenter l'imperceptibilité) par l'intermédiaire d'une matrice de Hamming. Le caractère "DEL" est dissimulé dans l'image à la fin du message, marque donc la fin du message s'il y a assez de place sinon il n'est pas présent à la fin du message.
*\param img : Pointeur vers une structure "image" contenant l'ensemble des informations d'une image, dont certains de ces pixels seront modifiés pour dissimuler le message
*\param file : Pointeur vers le fichier contenant le message que l'on cherche à cacher dans l'image
*\return Un booléen qui prend la valeur "true" si l'encodage s'est déroulé sans problèmes, ou "false", s'il y a eu un problème
*/
boolean encode_upgrade_3(image *img, FILE* file);
#endif
