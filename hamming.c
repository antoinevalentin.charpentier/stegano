#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "hamming.h"
#include "matrix_vector.h"
#include "reading.h"

void calc_size_hamming_matrix(int* nb_lines, int* nb_col, image img, vector msg){
  *nb_lines = 2;
  *nb_col = pow(2,*nb_lines)-1;
  int nb_color =1;

  //on regarde le nombre de couleurs par pixels d'image : par défaut 1 couleur (pour les pgm)
  if(img.data_header.magic_number[1] == '6'){ //si c'est une image ppm en couleur
    nb_color = 3;
  }

  while((img.data_header.width * img.data_header.height * nb_color) > msg.nb_lines*(*nb_col)/(*nb_lines) ){
    (*nb_lines)+=1;
    *nb_col = pow(2,*nb_lines)-1;
  }
  (*nb_lines)-=1;
  *nb_col = pow(2,*nb_lines)-1;

}

boolean extract_N_bits(vector *res, vector v, unsigned int offset){
	//on regarde s'il n'y a pas assez de valeur dans le vecteur v à partir d'un certain offset pour remplir le vecteur res
	if(offset+res->nb_lines > v.nb_lines){
		//on remplit le vecteur res avec les bits du vecteur v à partir d'un certain offset
		for(unsigned int k = offset; k < offset+res->nb_lines;k++){
			if(k < v.nb_lines){
				res->vect[k-offset]= v.vect[k];
			}else{//si le vecteur v est fini, on complète alors le vecteur res avec des 0
				res->vect[k-offset]= 0;
			}

		}
		return true;
	}

	//on remplit le vecteur res avec les bits du vecteur v à partir d'un certain offset
	for(unsigned int k = offset; k < offset+res->nb_lines;k++){
		res->vect[k-offset]= v.vect[k];
	}

	return true;
}



int** generate_hamming_matrix_v2(unsigned int nb_lines, unsigned int nb_col){
	int** res;
	int value_col, power_2;

	//on alloue la mémoire
	res = (int**)malloc(nb_lines*sizeof(int*));

	for(unsigned int i=0;i<nb_lines;i++){
		res[i] = (int*)malloc((nb_col)*sizeof(int));
	}

	//on remplit la matrice de Hamming
	for(unsigned int k = 0;k<nb_col;k++){
		value_col = k+1;
		power_2 = 1;
		for(unsigned int j = 0;j<nb_lines;j++){
			if((value_col & power_2) != 0){
				res[j][k] = 1;
			}else{
				res[j][k] = 0;
			}
			power_2 *= 2;
		}
	}
	//printf_matrix_2D(res, nb_lines, nb_col);
	//on return la matrice de hamming
	return res;
}

vector extract_LSB_pixels(image img){
	//fonction qui extrait les LSB des pixels de l'image dans un tableau dynamique
	vector res;
	int nb_color = 1;

	//on regarde le nombre de couleurs par pixels d'image : par défaut 1 couleur (pour les pgm)
	if(img.data_header.magic_number[1] == '6'){ //si c'est une image ppm en couleur
		nb_color = 3;
	}

	res.vect = (int*)malloc((img.data_header.width*img.data_header.height*nb_color)*sizeof(int));
	for(unsigned int k=0; k<img.data_header.width*img.data_header.height*nb_color;k++){
		res.vect[k] = (img.pixels[k])&1;//on ne met que le LSB du pixel
		//printf("%d\n", img.pixels[k]);
	}

	res.nb_lines = img.data_header.width*img.data_header.height*nb_color;
	return res;
}

vector extract_bits_msg(FILE* file ,image img){

	vector res;
	int value_cursor_file = -1, power_2;
	int nb_color =1;

	res.nb_lines = 0;
	res.vect = (int*)malloc((res.nb_lines+1)*sizeof(int));

	 //on regarde le nombre de couleurs par pixels d'image : par défaut 1 couleur (pour les pgm)
	if(img.data_header.magic_number[1] == '6'){ //si c'est une image ppm en couleur
		nb_color = 3;
	}



	while(feof(file) == 0){
		//on lit un octet
		fread(&value_cursor_file, 1, 1, (FILE*)file);
		power_2 = 128;

		if(feof(file)){//on regarde si on a fini de lire le msg caché
			if(img.data_header.height * img.data_header.width * nb_color - res.nb_lines >= 8){//s'il y a de la place pour mettre le caractère de fin dans l'image
				value_cursor_file = DEL;
			}else{
				value_cursor_file = -1;
			}
		}


		if(value_cursor_file != -1){
			(res.vect) = (int*)realloc(res.vect, ((res.nb_lines)+8)*sizeof(int));
			for(unsigned int k=0; k<8;k++){
				if((value_cursor_file&power_2) == power_2){
					res.vect[res.nb_lines+k] = 1;
				}else{
					res.vect[res.nb_lines+k] = 0;
				}
				power_2 /= 2;
			}
			res.nb_lines = res.nb_lines + 8;
		}
	}

	return res;
}

void correct_LSB_image(vector vec, image *img, int offset){
	int nb_col = 0;
	//l'offset sert à s'occuper des pixels par "groupe" et non modifier l'ensemble des pixels
	//on initialise le random
	srand(time(NULL));
	//on parcourt l'ensemble des composantes du vecteur Hv+r, pour savoir quelle colonne de la matrice de hamming prendre
	for(int k =vec.nb_lines-1;k>=0;k--){//on calcule l'indice de la colonne de la matrice de Hamming
		nb_col = nb_col*2+vec.vect[k];
	}

	if(nb_col != 0){//si un bit a besoin d'être modifié
		if(img->pixels[offset+nb_col-1] == img->data_header.max_value_pixel){//on regarde s'il a atteint la valeur maximale
			img->pixels[offset+nb_col-1] = (img->pixels[offset+nb_col-1]) - 1;///on retire alors 1 car on ne peut pas ajouter 1, sinon il serait supérieur à la valeur maximale
		}else if(img->pixels[offset+nb_col-1] == 0){//la valeur du pixel ne peut pas prendre de valeur négative, on ajoute alors +1 au lieu de faire -1
			img->pixels[offset+nb_col-1] = (img->pixels[offset+nb_col-1]) + 1;
		}else{
			//si le +-1 ne peut pas générer de débordement, alors on fait un random entre +1 et -1
			if(rand()%2 == 0){
				img->pixels[offset+nb_col-1] = (img->pixels[offset+nb_col-1]) - 1;
			}else{
				img->pixels[offset+nb_col-1] = (img->pixels[offset+nb_col-1]) + 1;
			}
		}
	}

}
