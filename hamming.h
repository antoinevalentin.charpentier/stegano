/*!
*\file hamming.h
*
*\brief Ensemble de fonction générant une matrice de Hamming et nécessaire pour cacher le message dans l'image
*
*\version 1.0
*\date 26/12/2020
*\author Antoine-Valentin CHARPENTIER & Bastien IVSAK
*/

#ifndef HAMMING_H
#define HAMMING_H

#include "utils.h"

/*!
*\brief Procédure qui permet de calculer la taille de la matrice de contrôle (Hamming) en fonction de la taille du message et de la taille de l'image.
*\param nb_lines : Pointeur vers une variable entière qui va accueillir le nombre de lignes de la matrice à la sortie de la procédure
*\param nb_col : Pointeur vers une variable entière qui va accueillir le nombre de colonnes de la matrice à la sortie de la procédure
*\param img : Structure contenant l'ensemble des informations d'une image
*\param msg : Vecteur contenant l'ensemble des bits du message à cacher dans l'image
*/
void calc_size_hamming_matrix(int* nb_lines, int* nb_col, image img, vector msg);
/*!
*\brief Fonction qui permet d'extraire N valeurs d'un vecteur dans un autre
*\param res : Vecteur résultat de l'opération d'extraction, contenant les valeurs extraites du vecteur v à partir d'un certain décalage
*\param v : Vecteur dont on cherche à extraire une partie de ces valeurs
*\param offset : Décalage d'indice dans l'extraction, qui permet de commencer l'extraction à un certain indice et non forcement à l'indice 0
*\return Un booléen qui prend la valeur "true" quand la fonction s'est terminée
*/
boolean extract_N_bits(vector *res, vector v, unsigned int offset);
/*!
*\brief Fonction qui permet de générer la matrice de contrôle (Hamming)
*\param nb_lines : Nombre de lignes de la matrice de Hamming
*\param nb_col : Nombre de colonnes de la matrice de Hamming
*\return La matrice de contrôle (Hamming) sous la forme d'un tableau deux dimensions d'entiers.
*/
int** generate_hamming_matrix_v2(unsigned int nb_lines, unsigned int nb_col);
/*!
*\brief Fonction qui permet d'extraire les LSB des pixels d'une image dans un vecteur, dont son contenu est stocké dans une structure de type "image"
*\param img : Structure contenant l'ensemble des informations d'une image
*\return Un vecteur contenant l'ensemble des LSB des pixels de l'image
*/
vector extract_LSB_pixels(image img);
/*!
*\brief Fonction qui permet d'extraire les bits d'un message contenu dans un fichier, et rajoute à la fin de la suite de bits le caractère de fin de message qui est le caractère "DEL" sous la forme de bits
*\param file : Pointeur vers le fichier dont nous cherchons à extraire son contenu
*\param img : Structure contenant l'ensemble des informations d'une image
*\return Un vecteur contenant l'ensemble des bits du message
*/
vector extract_bits_msg(FILE* file ,image img);
/*!
*\brief Fonction qui permet de modifier les LSB d'une image grâce au syndrome afin de dissimuler le message caché
*\param vec : Vecteur résultat du calcul du "pseudo-syndrome" H*v+r
*\param img : Pointeur vers une structure "image" contenant l'ensemble des informations d'une image, dont certains de ces pixels seront modifiés pour dissimuler le message
*\param offset : Décalage d'indice dans la correction des bits, qui permet de commencer la modification à un certain indice et non forcément à l'indice 0
*\return Un vecteur contenant l'ensemble des bits du message
*/
void correct_LSB_image(vector vec, image *img, int offset);

#endif
