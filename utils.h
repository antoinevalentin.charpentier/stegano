/*!
*\file utils.h
*
*\brief Bibliothèque avec l'ensemble des structures de données utilisées dans le programme, et des procédures utiles au cours de l'exécution du programme, comme des affichages ou libération de mémoire
*
*\version 1.0
*\date 26/12/2020
*\author Antoine-Valentin CHARPENTIER & Bastien IVSAK
*/

#ifndef UTILS_H
#define UTILS_H

/*!
*\def DEL
*\brief Valeur ASCII du caractère d'effacement DEL, qui marque la fin du message caché dans l'image si l'image est assez grande pour pouvoir ajouter ce caractère
*/
#define DEL 127

/*!
*\enum boolean
*\brief Variable booléenne qui peut prendre soit la valeur "true" ou la valeur "false"
*/
typedef enum {
  false,
  true
} boolean;

/*!
*\struct header
*\brief Stocke l'ensemble des informations présentes dans l'entête d'une image PPM ou PGM. C'est-à-dire le numéro magique ("P5","P6"), sa taille (largeur et hauteur en pixels), la valeur maximale que les pixels peuvent atteindre, ainsi que le nombre de lignes de commentaire présent dans l'entête.
*/
typedef struct {
  char magic_number[2]; /*!<Contient le numéro magique de l'image : dans notre cas soit P5 ou P6.*/
  unsigned int width;/*!<Largeur de l'image*/
  unsigned int height;/*!<Hauteur de l'image*/
  unsigned int max_value_pixel;/*!<Valeur maximale qu'un pixel de l'image peut atteindre */
  unsigned int nb_comment;/*!<On stocke le nombre de lignes de commentaire, car lors de la réécriture de l'image, on va faire une copie de l'entête et non le régénérer pour éviter d'avoir une variation du poids de l'image entre celle de base et celle avec le message caché. */
} header;

/*!
*\struct image
*\brief Stocke l'ensemble des informations liées à une image. C'est-à-dire son entête et les valeurs des pixels de l'image.
*/
typedef struct {
  header data_header; /*!<Permet de stocker l'entête de l'image*/
  unsigned int* pixels;/*!<Permet de stocker les pixels de l'image*/
} image;

/*!
*\struct Key
*\brief Stocke l'ordre de parcours des pixels généré par une clé (amélioration 2), ainsi que la longueur de cet ordre.
*/
typedef struct{
  unsigned int *order_bits;/*!<Stocke l'ordre du parcours des bits généré à partir d'une clé (amélioration2)*/
  unsigned int lenght;/*!<Longueur de la clé, qui est aussi la longueur du tableau contenant l'ordre de parcours des bits*/
} Key;

/*!
*\struct vector
*\brief Stocke un vecteur qui est un ensemble d'entiers ainsi que son nombre de composantes.
*/
typedef struct{
  unsigned int nb_lines;/*!<Nombre de lignes du vecteur*/
  int *vect;/*!<Stocke les différentes composantes du vecteur*/
} vector;

/*!
*\brief Procédure qui permet de libérer l'espace mémoire alloué dynamiquement à une image.
*\param img : Pointeur vers une structure "image" qui verra la mémoire de ces pixels désalloué.
*/
void destroy_pixels_image(image *img);
/*!
*\brief Procédure qui permet de libérer l'espace mémoire alloué dynamiquement à un vecteur.
*\param vect: Pointeur vers une structure "vector" qui verra la mémoire de ces composantes désallouée.
*/
void destroy_vector(vector *vect);

/*!
*\brief Procédure qui permet de libérer l'espace mémoire alloué dynamiquement à une matrice de contrôle (Hamming)
*\param H : Matrice de contrôle (Hamming) que l'on souhaite désallouer
*\param nb_lines : nombre de lignes de la matrice de Hamming
*/
void destroy_hamming_matrix(int** H, unsigned int nb_lines);

/*!
*\brief Procédure qui permet de libérer l'espace mémoire alloué dynamiquement à une clé.
*\param key: Pointeur vers une structure "Key" dont on cherche à désallouer la mémoire contenant le parcours des bits aléatoire.
*/
void destroy_key(Key *key);

/*!
*\brief Procédure qui permet d'afficher dans la console l'entête d'une image stockée dans une structure "image"
*\param img : L'image dont on souhaite afficher son entête dans la console
*/
void printf_header(image img);
/*!
*\brief Procédure qui permet d'afficher dans la console un vecteur
*\param v : Le vecteur que l'on souhaite afficher dans la console
*/
void printf_vector(vector v);
/*!
*\brief Procédure qui permet d'afficher dans la console une matrice de dimension 2
*\param matrix : la matrice de dimension 2 que l'on souhaite afficher
*\param nb_lines : le nombre de lignes de la matrice
*\param nb_col : le nombre de colonnes de la matrice
*/
void printf_matrix_2D(int **matrix, unsigned int nb_lines, unsigned int nb_col);

#endif
