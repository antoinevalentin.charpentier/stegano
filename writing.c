#include <stdio.h>

#include "writing.h"

void rewrite_header(FILE* file, image img, char* url){
  //on recopie l'entête de l'image pour éviter d'avoir une variation de la taille de l'image
  FILE* original_file;
  char c;
  int nb_lignes_header = 1+1+1+img.data_header.nb_comment; //3 lignes minimum dans l'entête (numéro magique, la taille de l'image, l'intensité maximale des pixels) + le nombre de lignes contenue dans l'entête

  //on ouvre le fichier d'origine pour faire une copie de son entête dans un nouveau fichier
  original_file = fopen(url, "r");

  //on parcourt toutes les lignes de l'entête
  for(int i=0;i<nb_lignes_header;i++){
    c = getc(original_file);//on récupère un caractère issu de l'entête d'origine
    putc(c,file);//on l'écrit dans le fichier de sortie

    //on recopie toute la ligne de l'image de base dans l'image de sortie
    while(c != '\n'){
      c = getc(original_file);
      putc(c,file);
    }
  }

  //on ferme l'image d'origine car on n'en a plus besoin
  fclose(original_file);

}

void write_pixels(FILE* file, image img){
  int nb_color = 1;

  //on regarde le nombre couleurs par pixels d'image : par défaut 1 couleur (pour les pgm)
  if(img.data_header.magic_number[1] == '6'){ //si c'est une image ppm en couleur
    nb_color = 3;
  }

  //on parcourt l'ensemble des pixels de l'image
  for( unsigned int k = 0; k < img.data_header.height * img.data_header.width * nb_color; k++) {
    fputc(img.pixels[k], file);
  }

}

boolean write_image(image img, char* url){
  FILE* file;

  /* ETAPE n°1 :  On ouvre le fichier de sortie */
  if(img.data_header.magic_number[0] == 'P' && img.data_header.magic_number[1] == '6'){
    file = fopen("images/output.ppm","wb");
  }else{
    file = fopen("images/output.pgm","wb");
  }

  if(file == NULL){
    printf("Erreur lors de l'ouverture de l'image\n");
    return false;
  }

  /* ETAPE n°2 :  On fait une copie de l'entête de l'ancienne image dans la nouvelle */
  rewrite_header(file, img, url);

  /* ETAPE n°3 :  On écrit les nouveaux pixels avec le message caché */
  write_pixels(file, img);

  /* ETAPE n°4 :  On ferme le fichier de sortie */
  fclose(file);

  return true;
}
