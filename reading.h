/*!
*\file reading.h
*
*\brief Bibliothèque pour pouvoir lire les données d'une image et les stocker dans une structure "image"
*
*\version 1.0
*\date 26/12/2020
*\author Antoine-Valentin CHARPENTIER & Bastien IVSAK
*/

#ifndef READING_H
#define READING_H

#include "utils.h"

/*!
*\brief Procédure qui permet de ne pas prendre en compte les commentaires et de compter le nombre de lignes de commentaire présent dans l'entête d'une image PPM ou PGM
*\param file : Pointeur vers le fichier contenant l'image dont nous cherchons à ne pas prendre en compte les commentaires présents dans son entête
*\param img : Pointeur vers une structure qui contient les informations de l'image, afin d'incrémenter le nombre de lignes de commentaire présent dans son entête
*/
void skip_comment(FILE* file, image *img);
/*!
*\brief Fonction qui permet de lire les informations présentes dans l'entête de l'image et de les stocker dans une structure "image"
*\param file : Pointeur vers le fichier contenant l'image dont nous cherchons à lire son entête
*\param img : Pointeur vers une structure qui contient les informations de l'image, afin de pouvoir stocker les informations de l'entête de cette image (numéro magique, taille, valeur maximale des pixels, ...)
*\return Un booléen qui prend la valeur "true" si la lecture de l'entête s'est déroulée sans problèmes, ou "false", s'il y a eu un problème
*/
boolean read_header_image(FILE* file, image* img);
/*!
*\brief Fonction qui permet de lire l'ensemble des valeurs des pixels de l'image et de les stocker dans une structure "image"
*\param file : Pointeur vers le fichier contenant l'image dont nous cherchons à lire les valeurs de ses pixels
*\param img : Pointeur vers une structure qui contient les informations de l'image, afin de pouvoir stocker les différentes valeurs de pixels
*\return Un booléen qui prend la valeur "true" si la lecture des pixels s'est déroulée sans problèmes, ou "false", s'il y a eu un problème
*/
boolean read_pixels_image(FILE* file, image* img);
/*!
*\brief Procédure qui permet de lire une chaîne de caractères saisie par l'utilisateur dans la console, que l'on stocke dans un tableau dynamique dont la taille augmente à chaque caractère saisi
*\param url : Chaîne de caractères qui va recevoir la chaîne de caractères saisie par l'utilisateur
*\param printf_text : Texte affiché dans la console pour pouvoir informer l'utilisateur qu'une saisie est attendue
*/
void read_text_console(char **url, char* printf_text);
/*!
*\brief Fonction qui permet d'ouvrir un fichier à partir de son chemin d'accès, dans un certain mode
*\param url : Chaîne de caractères qui va contenir le chemin d'accès au fichier que l'on souhaite ouvrir
*\param mode : Chaîne de caractères qui indique le mode dans lequel on doit ouvrir le fichier ("w","r",...)
*\param printf_text_url : Texte affiché dans la console pour pouvoir informer l'utilisateur qu'une saisie est attendue
*\return Pointeur vers le début du fichier qui vient d'être ouvert
*/
FILE* open_file(char **url, char* mode, char* printf_text_url);
/*!
*\brief Fonction qui permet de lire l'ensemble des informations d'une image et les stocker dans une structure "image"
*\param img : Pointeur vers une structure "image" qui va contenir l'ensemble des informations de l'image que l'on va lire
*\param url : Chaîne de caractères qui va contenir le chemin d'accès à l'image
*\param printf_text_url : Texte affiché dans la console pour pouvoir informer l'utilisateur qu'une saisie est attendue pour demander à l'utilisateur le chemin d'accés à l'image
*\return Un booléen qui prend la valeur "true" si la lecture de l'image s'est déroulée sans problèmes, ou "false", s'il y a eu un problème
*/
boolean read_image(image* img, char **url, char *printf_text_url);

#endif
