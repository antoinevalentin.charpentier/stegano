/*!
*\file main.c
*
*\brief Mise en place du projet de Stéganographie. Ce fichier fait appel aux fonctions d'encodage ou de décodage du message dans le fichier selon le souhait de l'utilisateur.
*
*\version 1.0
*\date 26/12/2020
*\author Antoine-Valentin CHARPENTIER & Bastien IVSAK
*/

/*!
*\mainpage Stéganographie : Cacher des messages dans des images
*INTRODUCTION :
  - La stéganographie regroupe l'ensemble des techniques, afin de rendre inaperçu un message au sein d'un autre. Le message que l'on cherche à dissimuler comporte généralement des données sensibles, tandis que le fichier qui va contenir le message caché est sans valeur.
  Dans notre cas, nous avons traité le cas de cacher tout type de message (document texte, image, document Word, ...) dans une image au format PPM ou PGM.
*/

#include <stdio.h>

#include "encode.h"
#include "decode.h"

int main(){

  int choice;

  //on demande à l'utilisateur s'il veut cacher son message dans l'image, ou au contraire s'il veut le récupérer
  do{
    printf("Quelle operation voulez vous effectuer ? 1 = encode, 2 = decode, 0 = quitter\n");
    scanf("%d",&choice);

    switch(choice){
      case 1:
        start_encode();
        break;
      case 2:
        start_decode();
        break;
      default:
        break;
    }

  }while(choice != 0);

  return 0;
}
