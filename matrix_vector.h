/*!
*\file matrix_vector.h
*
*\brief Bibliothèque pour pouvoir réaliser quelques opérations sur des matrices ou sur des vecteurs
*
*\version 1.0
*\date 26/12/2020
*\author Antoine-Valentin CHARPENTIER & Bastien IVSAK
*/

#ifndef MATRIX_VECTOR_H
#define MATRIX_VECTOR_H

#include "utils.h"

/*!
*\brief Fonction qui permet de réaliser la multiplication d'une matrice "matrix" par un vecteur "vec"
*\param matrix : la matrice qui va être multipliée par le vecteur
*\param vec : le vecteur qui va multiplier la matrice
*\param nb_col_matrix : le nombre de colonnes de la matrice
*\param res : Pointeur vers un vecteur qui va accueillir le résultat de la multiplication de la matrice par le vecteur
*\return Un booléen qui prend la valeur "true" si la multiplication s'est déroulée sans problèmes, ou "false", s'il y a eu un problème
*/
boolean multiply_matrix_vector(int **matrix, vector vec, unsigned int nb_col_matrix, vector *res);
/*!
*\brief Fonction qui permet de réaliser une addition modulo 2 entre deux vecteurs
*\param vect1 : le premier vecteur de l'addition
*\param vect2 : le deuxième vecteur de l'addition
*\param res : Pointeur vers un vecteur qui va accueillir le résultat de l'addition modulo 2 des deux vecteurs
*\return Un booléen qui prend la valeur "true" si l'addition modulo 2 s'est déroulée sans problèmes, ou "false", s'il y a eu un problème
*/
boolean adition_modulo_2(vector vect1, vector vect2, vector *res);
/*!
*\brief Fonction qui permet de réaliser le calcul H*v+r où H est une matrice et v,r deux vecteurs
*\param matrix : désigne la matrice H dans l'opération H*v+r
*\param nb_col_matrix : désigne le nombre de colonnes de la matrice H
*\param v : désigne le vecteur v dans l'opération H*v+r
*\param r : désigne le vecteur r dans l'opération H*v+r
*\param res : Pointeur vers un vecteur qui va accueillir le résultat du calcul H*v+r où H est une matrice et v,r deux vecteurs
*\return Un booléen qui prend la valeur "true" si les calculs se sont déroulés sans problèmes, ou "false", s'il y a eu un problème
*/
boolean calc_Hv_r(int **matrix, int nb_col_matrix, vector v,vector r, vector *res);

#endif
