#include <stdio.h>
#include <stdlib.h>

#include "utils.h"

void destroy_pixels_image(image *img){
  if(img->pixels){ //on regarde si les pixels ont bien été alloués
    free(img->pixels);//libération de la mémoire du tableau dynamique dédiée aux pixels de l'image si elle a été allouée
  }
}

void destroy_key(Key *key){
  if(key->order_bits){ //on regarde si les pixels ont bien été alloués
    free(key->order_bits);//libération de la mémoire du tableau dynamique dédiée aux pixels de l'image si elle a été allouée
  }
}

void destroy_vector(vector *v){
  if(v->vect){
    free(v->vect);
  }
}

void destroy_hamming_matrix(int** H, unsigned int nb_lines){
  if(H != NULL){

    for(unsigned int i=0;i<nb_lines;i++){
      if(H[i] != NULL){
        free(H[i]);
      }
    }

    free(H);
  }
}

void printf_header(image img){
  //Affichage dans la console de l'entête de l'image
  printf("\n============ ENTETE ==========\n");
  printf(">> Nombre magique : %s\n", img.data_header.magic_number);
  printf(">> Largeur : %u\n", img.data_header.width);
  printf(">> Hauteur : %u\n", img.data_header.height);
  printf(">> Valeur Max : %u\n", img.data_header.max_value_pixel);
  printf(">> Nombre de lignes de commentaire : %u\n", img.data_header.nb_comment);
  printf("==============================\n");
}

void printf_vector(vector v){
  //procédure qui affiche les composantes du vecteur
  printf("Debut affichage vecteur\n");
  for(unsigned int i=0;i<v.nb_lines;i++){
    printf("%d - ",v.vect[i]);
  }
  printf("\nFin affichage vecteur\n");
}

void printf_matrix_2D(int **matrix, unsigned int nb_lines, unsigned int nb_col){
  //procédure qui affiche une matrice 2D
  printf("Debut affichage matrice\n");
  for(unsigned int i=0;i<nb_lines;i++){
    for(unsigned int j=0;j<nb_col;j++){
      printf("%d - ",matrix[i][j]);
    }
    printf("\n");
  }
  printf("Fin affichage matrice\n");
}
