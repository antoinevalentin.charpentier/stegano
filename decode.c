#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "decode.h"
#include "reading.h"
#include "key.h"
#include "hamming.h"
#include "matrix_vector.h"

boolean decode_upgrade_1(image img, FILE* file){
  /*DECODAGE AMELIORATION 1*/

  int nb_color = 1; //on suppose dans un premier temps que c'est une image en noir et blanc
  unsigned int k = 0;
  unsigned char value_cursor_file = 0;

  //on regarde combien de couleurs par pixels d'image sont utilisées : par défaut 1 couleur (pour les pgm)
  if(img.data_header.magic_number[1] == '6'){ //si c'est une image ppm en couleur
    nb_color = 3;
  }

  boolean continuer = true;

  while(continuer == true){
    if(k%8 == 0 && k != 0){//si value_cursor_file a récupéré la valeur complète d'un octet du message caché dans l'image
    	fputc(value_cursor_file, file);//on ajoute le résultat obtenu dans le fichier de sortie
      value_cursor_file = 0;
    }

    value_cursor_file = value_cursor_file << 1;
    if((img.pixels[k] & 1) == 1){//on regarde l'état du LSB du pixel, s'il vaut 1 ou non, si c'est le cas on incrémente la valeur
		  value_cursor_file += 1;
    }

    k++;
    if(k >= img.data_header.width * img.data_header.height * nb_color){//si on a fait tous les pixels de l'image, on sort
    	continuer = false;
    }
    if(k%8 == 0){ //si on change de caractère
    	if(value_cursor_file == DEL || 8 > img.data_header.width * img.data_header.height * nb_color - k){//si on rencontre le caractère de fin ou il n'y a pas assez de place dans l'image pour un autre octet
        if(value_cursor_file != DEL){
          fputc(value_cursor_file, file);
        }
        continuer = false;
      }
    }
  }

  return true;
}

boolean decode_upgrade_2(image img, FILE* file){
  /*DECODAGE AMELIORATION 2*/
  int nb_color = 1; //on suppose dans un premier temps que l'image est en noir et blanc
  unsigned int k = 0;
  unsigned char value_cursor_file = 0;
  unsigned int pixel_number;

  //on récupère la clé du message, et on génère un ordre de parcours
  Key key = generate_bits_order();

  //on regarde combien de couleurs par pixels d'image sont utilisées: par défaut 1 couleur (pour les pgm)
  if(img.data_header.magic_number[1] == '6'){ //si c'est une image ppm en couleur
    nb_color = 3;
  }

  boolean continuer = true;

  while(continuer == true){
    pixel_number = key.order_bits[k%key.lenght]+(k/key.lenght)*key.lenght;

    if(k%8 == 0 && k != 0){//si value_cursor_file a récupéré la valeur complète d'un octet du message caché dans l'image
      fputc(value_cursor_file, file);//on ajoute le résultat obtenu dans le fichier de sortie
      value_cursor_file = 0;
    }

    value_cursor_file = value_cursor_file << 1;
    if((img.pixels[pixel_number] & 1) == 1){//on regarde l'état du LSB du pixel, s'il vaut 1 ou non, si c'est le cas on incrémente la valeur
      value_cursor_file += 1;
    }

    k++;
    if(k >= img.data_header.width * img.data_header.height * nb_color){//si on a fait tous les pixels de l'image, on sort
      continuer = false;
    }
    if(k%8 == 0){ //si on change de caractère
      if(value_cursor_file == DEL || 8 > img.data_header.width * img.data_header.height * nb_color - k){//si on rencontre le caractère de fin ou il n'y a pas assez de place dans l'image pour un autre octet
        if(value_cursor_file != DEL){
          fputc(value_cursor_file, file);
        }
        continuer = false;
      }
    }
  }

  destroy_key(&key);
  return true;
}


boolean decode_upgrade_3(image img, FILE* file){
  unsigned char value_cursor_file = 0;
  int nb_lines_hamming_matrix,nb_col_hamming_matrix;
  int nb_color =1;
  boolean continuer = true, status;

  //on regénère la matrice de contrôle (Hamming) utilisée pour cacher le message, pour avoir la même que celle utilisée lors de l'encodage du message dans l'image
  printf("Saisir le nombre de ligne de la matrice de Hamming : \n");
  scanf(" %d", &nb_lines_hamming_matrix);
  nb_col_hamming_matrix = pow(2,nb_lines_hamming_matrix)-1;

  int **H = generate_hamming_matrix_v2(nb_lines_hamming_matrix, nb_col_hamming_matrix);

  //on récupère l'ensemble des pixels de l'image
  vector lsb_pixels = extract_LSB_pixels(img);
  vector v, Hv;
  vector msg;

  //on alloue la mémoire au vecteur v et on regarde s'il n'y a pas eu de problème
  v.vect = (int*)malloc((nb_col_hamming_matrix)*sizeof(int));
  v.nb_lines = nb_col_hamming_matrix;

  if(v.vect == NULL){
    printf("Probleme d'allocation memoire\n");
    return false;
  }

  //on regarde le nombre de couleurs par pixels d'image : par défaut 1 couleur (pour les pgm)
  if(img.data_header.magic_number[1] == '6'){ //si c'est une image ppm en couleur
    nb_color = 3;
  }

  //on intialise les vecteurs contenant les bits
  msg.nb_lines = 0;
  msg.vect = (int*)malloc(1*sizeof(int));

  Hv.nb_lines = nb_lines_hamming_matrix;
  Hv.vect = (int *)malloc((nb_lines_hamming_matrix)*sizeof(int));


  for(unsigned int k=0;k<ceil((float)(img.data_header.width * img.data_header.height * nb_color)/nb_col_hamming_matrix);k++){

    //on extrait v des pixels de l'image
    extract_N_bits(&v,lsb_pixels,nb_col_hamming_matrix*k);

    //on calcule H*v
    status = multiply_matrix_vector(H, v, nb_col_hamming_matrix, &Hv);

    //s'il y a eu une erreur lors du calcul de H*v
    if(status == false){
      printf("Erreur : probleme lors du calcul de Hv\n");
      return false;
    }

    //on agrandit le vecteur msg du nombre de case de Hv
    msg.nb_lines = msg.nb_lines+Hv.nb_lines;
    msg.vect = realloc(msg.vect,msg.nb_lines*sizeof(int));

    //on stocke dans l'agrandissement du vecteur les bits retrouvés par le calcul de Hv
    for(unsigned int i=0;i<Hv.nb_lines;i++){
      msg.vect[k*Hv.nb_lines+i] = Hv.vect[i];
    }

  }

  //on recrée le fichier message, à partir des bits du message retrouvés
  unsigned int k =0;
  while(continuer == true){

    if(k%8 == 0 && k != 0){//si value_cursor_file a récupéré la valeur complète d'un octet du message caché dans l'image
      fputc(value_cursor_file, file);
      value_cursor_file = 0;
    }

    value_cursor_file = value_cursor_file << 1;
    if(msg.vect[k] == 1){//on regarde l'état du LSB du pixel, s'il vaut 1 ou non, si c'est le cas on incrémente la valeur
      value_cursor_file += 1;
    }

    k++;
    if(k >= msg.nb_lines){//si on a fait tous les bits du message, on sort
      continuer = false;
    }
    if(k%8 == 0){ //si on change de caractère
      if(value_cursor_file == DEL || 8 > msg.nb_lines - k){//si on rencontre le caractère de fin ou il n'y a pas assez de place dans le vecteur pour un autre octet
        if(value_cursor_file != DEL){
          fputc(value_cursor_file, file);
        }
        continuer = false;
      }
    }
  }

  //on libère la mémoire allouée
  destroy_vector(&lsb_pixels);
  destroy_vector(&msg);
  destroy_vector(&v);
  destroy_vector(&Hv);
  destroy_hamming_matrix(H, nb_lines_hamming_matrix);

  return true;
}

boolean start_decode(){
  //Stockage des chemins d'accés aux fichiers
  char* url_image;
  char* url_msg;
  int choice;//pour le menu de sélection de l'amélioration

  //Initialisation de la structure qui va contenir l'image avec le message caché
  image img = {.data_header = {.nb_comment = 0}};

  //Initialisation de booléens
  boolean read_status, decode_status = true;

  ///On récupère les informations de l'image avec le message caché
  read_status = read_image(&img, &url_image, "Saisir le chemin d'acces de l'imade a decoder (images/output.pgm ou images/output.ppm) : ");

  //s'il y a eu un problème lors de la lecture de l'image
  if(read_status == false){
    fprintf(stderr, "Invalid image\n");
    return false;
  }

  //Affichage dans la console de l'entête de l'image que l'on vient de lire
  printf_header(img);

  //on demande à l'utilisateur le chemin d'accès au fichier dans lequel va être stocké le message caché que l'on a récupéré de l'image
  FILE* file = open_file(&url_msg, "wb", "Saisir le chemin d'acces dans lequel va etre stocke le message de sortie (ex : output.txt): ");

  //on regarde s'il n'y a pas eu de problème lors de l'ouverture du fichier contenant le texte
  if(file == NULL){
    destroy_pixels_image(&img);
    //on libère la mémoire allouée à l'url pour accéder au texte
    if(url_msg){
      free(url_msg);
    }
    printf("Problème lors de l'ouverture du fichier en mode écriture\n");
    return false;
  }

  //on demande à l'utilisateur quelle amélioration il souhaite utiliser pour retrouver son message dans l'image
  do{
    printf("\nQuelle amelioration voulez vous utiliser ? 1 = amelioration 1, 2 = amelioration 2, 3 = amelioration 3\n");
    scanf("%d",&choice);

    switch(choice){
      case 1:
        //on retrouve le message caché dans l'image caché avec l'amélioration 1
        decode_status = decode_upgrade_1(img,file);
        break;
      case 2:
        //on retrouve le message caché dans l'image caché avec l'amélioration 2
        decode_status = decode_upgrade_2(img,file);
        break;
      case 3:
        //on retrouve le message caché dans l'image caché avec l'amélioration 3
        decode_status = decode_upgrade_3(img,file);
        break;
      default:
        break;
    }
  }while(choice != 1 && choice != 2 && choice != 3);

  //s'il y a eu un problème lors du décodage du message, on affiche un message dans la console
  if(decode_status == false){
    printf("Probleme lors du decodage du message cache dans l'image\n");
  }

  //on libére la mémoire allouée par allocation dynamique pour les pixels de l'image
  destroy_pixels_image(&img);

  //on libère la mémoire allouée à l'url pour accéder au texte
  printf("\nLe fichier vient d'etre genere a l'adresse : %s\n",url_msg);
  if(url_msg){
    free(url_msg);
  }

  //on libère la mémoire allouée à l'url pour accéder à l'image
  if(url_image){
    free(url_image);
  }

  //on ferme le fichier texte, on n'en a plus besoin
  fclose(file);

  return decode_status;
}
