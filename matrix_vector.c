#include <stdio.h>
#include <stdlib.h>

#include "matrix_vector.h"

/*Des fonctions basiques entre matrice et vecteur utilisées pour l'amélioration 3*/

boolean multiply_matrix_vector(int **matrix, vector vec, unsigned int nb_col_matrix, vector *res){
	int value;
	//une matrice est définie par matrix[lignes][colonnes]

	//multiplication impossible ?
	if(nb_col_matrix != vec.nb_lines){
		printf("multiplication impossible\n");
		return false;
	}

	//on regarde si cela a bien été allouée
	if(res->vect == NULL){
		printf("probleme lors de l'allocation du tableau dynamique\n");
		return false;
	}

	//on calcule les composantes du vecteur issues de la multiplication de la matrice par le vecteur
	for(unsigned int i=0;i<res->nb_lines;i++){
		value = 0;
		for(unsigned int j=0;j<nb_col_matrix;j++){
			value += (matrix[i][j])*(vec.vect[j]);
		}
		res->vect[i] = value%2;
	}

	return true;
}

boolean adition_modulo_2(vector vect1, vector vect2, vector *res){

	//addition impossible ?
	if(vect1.nb_lines != vect2.nb_lines){
		printf("addition module 2 impossible\n");
		return false;
	}

	//on calcule les composantes du vecteur de sortie
	for(unsigned int i =0;i<res->nb_lines;i++){
		res->vect[i] = (vect1.vect[i]+vect2.vect[i])%2;
	}

	return true;
}


boolean calc_Hv_r(int **matrix, int nb_col_matrix,vector v,vector r, vector *res){
	boolean status;

	//on réalise dans un premier la multiplication H*v
	status = multiply_matrix_vector(matrix, v, nb_col_matrix, res);

	if(status == false){
		fprintf(stderr,"Erreur : Probleme lors de la multiplication (calc_Hv_r)\n");
		return false;
	}

	//puis on fait l'addition avec r
	status = adition_modulo_2(*res, r, res);

	if(status == false){
		fprintf(stderr,"Erreur  :Probleme lors de l'addition modulo 2 (calc_Hv_r)\n");
		return false;
	}

	//on return le résultat du calcul H*v+r
	return true;
}
