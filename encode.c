#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "writing.h"
#include "reading.h"
#include "encode.h"
#include "key.h"
#include "hamming.h"
#include "matrix_vector.h"

boolean encode_upgrade_1(image *img, FILE* file){
  /*ENCODAGE AVEC L'AMELIORATION 1*/
  int nb_color = 1; //on suppose dans un premier temps que l'image est en noir et blanc
  int value_cursor_file = 0, power_2 = 1; //la valeur étudiée qui se déplace dans le message que l'on cherhe à cacher
  unsigned int k = 0, cpt_last = 0; //des compteurs

  //on regarde le nombre de couleurs par pixels d'image : par défaut 1 couleur (pour les pgm)
  if(img->data_header.magic_number[1] == '6'){ //si c'est une image ppm en couleur
    nb_color = 3;
  }

  //on initialise le random
  srand(time(NULL));

  //on parcourt l'ensemble des pixels de l'image, et on s'arrête quand on a fini de cacher le msg
  while(k < img->data_header.height * img->data_header.width * nb_color && cpt_last < 8){
    if(!feof(file)){ //si on n'a pas fini de lire le fichier contenant le message
      if(k % 8 == 0){ ///si on a mis les 8 bits du nombre dans l'image, on change de nombre
        fread(&value_cursor_file, 1, 1, (FILE*)file); //on récupère l'octet suivant, que l'on stocke dans value_cursor_file
        power_2 = 128;
        if(img->data_header.height * img->data_header.width * nb_color - k < 8 && !feof(file)){//on regarde s'il y a assez de place pour stocker un octet complet en plus dans l'image
          printf("Erreur : il n'y a pas assez de place dans l'image pour stocker le message (%d => %d).\n",img->data_header.height * img->data_header.width * nb_color,k);
          return false;
        }
      }
    }

    if(feof(file)){//s'il n'y a plus de message à cacher, on écrit dans l'image le caractère de fin de fichier sur 8 pixels, et s'il y a assez de place dans l'image pour stocker un octet de plus (caractère de fin du message)
      if(cpt_last == 0 && img->data_header.height * img->data_header.width * nb_color - k >= 8){//si c'est la première fois qu'il boucle alors qu'il a fini de mettre le msg dans l'image, on regarde s'il y a assez de place pour mettre le caractère de fin
        value_cursor_file = DEL;
        power_2 = 128;
      }else if(cpt_last == 0){//s'il n'y a pas assez de place dans l'image pour mettre le caractère de fin, on ne l'écrit donc pas
        value_cursor_file = -1;
      }
      cpt_last++;
    }

    if(value_cursor_file != -1){
      //(value_cursor_file & power_2) == power_2 équivaut à dire que le bit d'information est égal à 1
      if(((value_cursor_file & power_2) == power_2 && (img->pixels[k] & 1) == 0)||((value_cursor_file & power_2) == 0 && (img->pixels[k] & 1) == 1)){ //si le bit d'information et le LSB du pixel ne sont pas égaux
        //on gère dans un premier temps les éventuels débordements des valeurs que le pixel peut prendre
        if(img->pixels[k] == img->data_header.max_value_pixel){//on regarde s'il a atteint la valeur maximale
          img->pixels[k] = (img->pixels[k]) - 1;///on retire alors 1 car on ne peut pas ajouter 1 car sinon il serait plus grand que la valeur maximale
        }else if(img->pixels[k] == 0){//la valeur du pixel ne peut pas prendre de valeur négative, on ajoute alors +1 au lieu de faire -1
          img->pixels[k] = (img->pixels[k]) + 1;
        }else{
          //si le +-1 ne peut pas générer de débordement, alors on fait un random entre +1 et -1
          if(rand()%2 == 0){
            img->pixels[k] = (img->pixels[k]) - 1;
          }else{
            img->pixels[k] = (img->pixels[k]) + 1;
          }
        }
      }

      power_2 /= 2;
      k++;
    }

  }
  printf("\n");

  //s'il n'y a pas eu de problème lors de l'encodage du texte dans l'image
  return true;
}

boolean encode_upgrade_2(image *img, FILE* file){
  /*ENCODAGE AVEC L'AMELIORATION 2*/
  int nb_color = 1; //on suppose dans un premier temps que l'image est en noir et blanc
  int value_cursor_file = 0, power_2 = 1; //la valeur étudiée qui se déplace dans le message que l'on cherhe à cacher
  unsigned int k = 0, cpt_last = 0; //des compteurs
  unsigned int pixel_number;

  //on recupère la clé du message, et on génère un ordre de parcours à partir de celle-ci
  Key key = generate_bits_order();

  //on regarde le nombre de couleurs par pixels d'image : par défaut 1 couleur (pour les pgm)
  if(img->data_header.magic_number[1] == '6'){ //si c'est une image ppm en couleur
    nb_color = 3;
  }

  //on initialise le random
  srand(time(NULL));

  //on parcourt l'ensemble des pixels de l'image, ou on s'arrête quand on a fini de cacher le msg
  while(k < img->data_header.height * img->data_header.width * nb_color && cpt_last < 8){
    if(!feof(file)){ //si on a pas fini de lire le fichier contenant le message
      if(k % 8 == 0){ ///si on a mis les 8 bits du nombre dans l'image, on change de nombre
        fread(&value_cursor_file, 1, 1, (FILE*)file); //on récupère l'octet suivant, que l'on stocke dans value_cursor_file
        power_2 = 128;
        if(img->data_header.height * img->data_header.width * nb_color - k < 8 && !feof(file)){//on regarde s'il y a assez de place pour stocker un octet complet en plus dans l'image
          printf("Erreur : il n'y a pas assez de place dans l'image pour stocker le message (%d => %d).\n",img->data_header.height * img->data_header.width * nb_color,k);
          destroy_key(&key);
          return false;
        }
      }
    }

    if(feof(file)){//s'il n'y a plus de message à cacher, on écrit dans l'image le caractère de fin de fichier sur 8 pixels, et s'il y a assez de place dans l'image pour stocker un octet de plus (caractère de fin du message)
      if(cpt_last == 0 && img->data_header.height * img->data_header.width * nb_color - k >= 8){//si c'est la première fois qu'il boucle alors qu'il a fini de mettre le msg dans l'image, on regarde s'il y a assez de place pour mettre le caractère de fin
        value_cursor_file = DEL;
        power_2 = 128;
      }else if(cpt_last == 0){//s'il n'y a pas assez de place dans l'image pour mettre le caractère de fin
        value_cursor_file = -1;
      }
      cpt_last++;
    }

    if(value_cursor_file != -1){
        //on regarde quel pixel va être concerné par l'éventuelle modification pour stocker le bit d'information
      //(k/key.lenght)*key.lenght; est différent de k, de plus cette partie permet que le parcours des pixels se répète si la clé est plus petite que le message
      pixel_number = key.order_bits[k%key.lenght]+(k/key.lenght)*key.lenght;
      //on regarde si le LSB du pixel est différent du bit d'information, si c'est le cas on doit le faire correspondre
      if(((value_cursor_file & power_2) == power_2 && (img->pixels[pixel_number] & 1) == 0)||((value_cursor_file & power_2) == 0 && (img->pixels[pixel_number] & 1) == 1)){ //si le bit d'information et le LSB du pixel ne sont pas égaux
        //on gère dans un premier temps les éventuels débordements des valeurs que le pixel peut prendre
        if(img->pixels[pixel_number] == img->data_header.max_value_pixel){//on regarde s'il a atteint la valeur maximale
          img->pixels[pixel_number] = (img->pixels[pixel_number]) - 1;///on retire alors 1 car on ne peut pas ajouter 1, sinon il serait plus grand que la valeur maximale
        }else if(img->pixels[pixel_number] == 0){//la valeur du pixel ne peut pas prendre de valeur négative, on ajoute alors +1 au lieu de faire -1
          img->pixels[pixel_number] = (img->pixels[pixel_number]) + 1;
        }else{
          //si le +-1 ne peut pas générer de débordement, alors on fait un random entre +1 et -1
          if(rand()%2 == 0){
            img->pixels[pixel_number] = (img->pixels[pixel_number]) - 1;
          }else{
            img->pixels[pixel_number] = (img->pixels[pixel_number]) + 1;
          }
        }
      }

      power_2 /= 2;
      k++;
    }
  }
  destroy_key(&key);
  printf("\n");
  return true;
}

boolean encode_upgrade_3(image *img, FILE* file){
  boolean status;
  int nb_col_hamming;
  int nb_lines_hamming;

  //on extrait les LSB des pixels de l'image et on les stocke dans le vecteur v
  vector lsb_pixels = extract_LSB_pixels(*img);

  //on extrait les bits du message caché et on les stocke dans le vecteur r
  vector bits_msg = extract_bits_msg(file,*img);

  //fonction qui calcule la taille idéale de la matrice de Hamming
  calc_size_hamming_matrix(&nb_lines_hamming, &nb_col_hamming, *img, bits_msg);

  //on initialise la matrice de Hamming
  int **H;
  H = generate_hamming_matrix_v2(nb_lines_hamming, nb_col_hamming);


  //On initialise les vecteurs qui vont accueillir les vecteurs : v, r, Hv+r
  vector v;
  v.nb_lines = nb_col_hamming;
  v.vect = (int*)malloc((v.nb_lines)*sizeof(int));

  vector r;
  r.nb_lines = nb_lines_hamming;
  r.vect = (int*)malloc((r.nb_lines)*sizeof(int));

  vector Hv_r;
  Hv_r.nb_lines = r.nb_lines;
  Hv_r.vect = (int*)malloc((Hv_r.nb_lines)*sizeof(int));

  //On regarde s'ils sont bien alloués
  if(v.vect == NULL || r.vect == NULL || Hv_r.vect == NULL){
    printf("Probleme d'allocation memoire (encode_upgrade_3)\n");
    return false;
  }

  int nb_color =1;
  //on regarde le nombre de couleurs par pixels d'image : par défaut 1 couleur (pour les pgm)
  if(img->data_header.magic_number[1] == '6'){ //si c'est une image ppm en couleur
    nb_color = 3;
  }

  //on regarde s'il y a assez de place dans l'image
  if((nb_col_hamming*bits_msg.nb_lines)/(nb_lines_hamming) >= img->data_header.width*img->data_header.height*nb_color){
    printf("Erreur : pas asez de place pour cacher le message\n");
    return false;
  }

  //On affiche dans la console la taille de la matrice de Hamming utilisée
  printf("\nTaille de la matrice de controle (Hamming) :\n");
  printf("    >> NB LIGNES = %d\n",nb_lines_hamming);
  printf("    >> NB COLONNES = %d\n",nb_col_hamming);
  printf("\nRemarque : Pour pouvoir decoder le message, vous devez transmettre le nombre de lignes de la matrice de Hamming utilisee pour encoder le message dans l'image.\n\n");

  //on cache le message dans l'image
  for(unsigned int k=0;k<ceil((float)(bits_msg.nb_lines)/nb_lines_hamming);k++){
    //on extrait v
    extract_N_bits(&v,lsb_pixels,nb_col_hamming*k);

    //on extrait r
    extract_N_bits(&r,bits_msg, r.nb_lines*k);

    //on calcule Hv+r
    status = calc_Hv_r(H, nb_col_hamming, v, r, &Hv_r);
    //on regarde s'il n'y a pas eu de problème lors du calcul de H*v+r
    if(status == false){
      printf("Erreur : probleme lors du calcul de Hv+r\n");
      return false;
    }

    //on corrige les bits de l'image qui ont une erreur
    correct_LSB_image(Hv_r, img, nb_col_hamming*k);
  }

  //On libère la mémoire
  destroy_vector(&lsb_pixels);
  destroy_vector(&bits_msg);
  destroy_vector(&v);
  destroy_vector(&r);
  destroy_vector(&Hv_r);
  destroy_hamming_matrix(H, nb_lines_hamming);

  return true;
}


boolean start_encode(){
  //Initialisation de la variable pour le menu de sélection entre les différentes améliorations
  int choice;
  //Initialisation de la variable qui va contenir l'url du fichier que l'on souhaite cacher dans l'image
  char* url_msg;
  //Initialisation de la structure qui va contenir l'ensemble des données de l'image d'origine puis qui va subir des modifications pour dissimuler le message caché
  image img = {.data_header = {.nb_comment = 0}};
  //Initialisation de booléens
  boolean read_status, encode_status;
  //Stockage du chemin d'accés de l'image d'origine
  char* url_image;

  //On récupère les informations de l'image de base
  read_status = read_image(&img, &url_image, "Saisir le chemin d'acces a l'image qui va contenir le message :");

  //s'il y a eu un problème lors de la lecture de l'image
  if(read_status == false){
    fprintf(stderr, "Invalid image\n");
    //on libère la mémoire allouée à l'url pour accéder au texte
    if(url_image != NULL){
      free(url_image);
    }
    return false;
  }

  //Affichage dans la console de l'entête de l'image que l'on vient de lire
  printf_header(img);

  //on demande à l'utilisateur le chemin d'accès au fichier contenant le message que l'on cherche à dissimuler
  FILE* file = open_file(&url_msg, "rb", "Saisir le chemin d'acces fichier contenant le message :");

  //on vérifie qu'il n'y a pas eu de problème lors de l'ouverture du fichier contenant le message
  if(file == NULL){
    printf("Erreur lors de l'ouverture du fichier contenant le message\n");
    destroy_pixels_image(&img);
    //on libère la mémoire allouée à l'url pour accéder au texte
    if(url_msg){
      free(url_msg);
    }
    //on libère la mémoire allouée à l'url pour accéder à l'image
    if(url_image){
      free(url_image);
    }
    return false;
  }

  //on demande à l'utilisateur quelle amélioration, il veut utiliser pour cacher son message dans l'image
  do{
    printf("Quelle amelioration voulez vous utiliser ? 1 = amelioration 1, 2 = amelioration 2, 3 = amelioration 3\n");
    scanf("%d",&choice);

    switch(choice){
      case 1:
        //on inscrit le message caché dans l'image en utilisant l'amélioration 1
        encode_status = encode_upgrade_1(&img, file);
        break;
      case 2:
        //on inscrit le message caché dans l'image en utilisant l'amélioration 2
        encode_status = encode_upgrade_2(&img, file);
        break;
      case 3:
        //on inscrit le message caché dans l'image en utilisant l'amélioration 3
        encode_status = encode_upgrade_3(&img, file);
        break;
      default:
        break;
    }
  }while(choice != 1 && choice != 2 && choice != 3);

  //s'il y a eu une erreur lors de l'encodage du message dans l'image, on affiche alors un message d'erreur
  if(encode_status == false){
    printf("Probleme lors de l'encodage du message dans l'image\n");
    destroy_pixels_image(&img);
    //on libère la mémoire allouée à l'url pour accéder au texte
    if(url_msg){
      free(url_msg);
    }
    //on libère la mémoire allouée à l'url pour accéder à l'image
    if(url_image){
      free(url_image);
    }
    return false;
  }

  //on ferme le fichier, on n'en a plus besoin
  fclose(file);

  //on écrit l'image avec les modifications
  write_image(img, url_image);

  //on affiche dans la console le chemin d'accès du fichier qui vient d'être généré (image de base avec le message caché à l'intérieur)
  if(img.data_header.magic_number[1] == '6'){
    printf("Le fichier vient d'etre genere a l'adresse : images/output.ppm\n");
  }else{
    printf("Le fichier vient d'etre genere a l'adresse : images/output.pgm\n");
  }

  //on libére la mémoire allouée par allocation dynamique pour les pixels de l'image
  destroy_pixels_image(&img);

  //on libère la mémoire alloué à l'url pour accéder au texte
  if(url_msg){
    free(url_msg);
  }

  //on libère la mémoire allouée à l'url pour accéder à l'image
  if(url_image){
    free(url_image);
  }

  return true;
}
