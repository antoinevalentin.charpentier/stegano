#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "reading.h"

void skip_comment(FILE* file, image *img){
  //pour ne pas "étudier" les commentaires, on les "passe".
  char c = getc(file);
  //s'il n'y a pas de commentaire, on décale d'un cran en arrière le pointeur pour qu'il n'y ait pas de décalage de pointeur, vu que la ligne précédente a décalé le pointeur d'un cran en avant
  if(c != '#'){
    fseek(file, -1, SEEK_CUR);
  }else{
    //si c'est un commentaire, on parcourt toutes les lignes des commentaires s'il y en a plusieurs d'affilé
    while(c == '#'){
      img->data_header.nb_comment++;
      //on parcourt toute la ligne du commentaire
      while(c != '\n'){
        c = getc(file);
      }
    }
  }
}

boolean read_header_image(FILE* file, image* img){

  //on récupère le "nombre magique", si on n'a pas réussi à le récupérer on affiche une erreur
  if(fscanf(file, "%c%c\n", &img->data_header.magic_number[0], &img->data_header.magic_number[1]) != 2){
    fprintf(stderr, "Impossible de lire le numero magique (il doit etre'P5' or 'P6')\n");
    return false;
  }
  //on regarde si le nombre magique est pris en charge par notre programme qui ne supporte que les fichiers ppm et pgm dont les pixels sont codés en binaire
  if(img->data_header.magic_number[0] != 'P' || (img->data_header.magic_number[1] != '5' && img->data_header.magic_number[1] != '6')){
    fprintf(stderr, "Invalide format du numero magique (il doit etre 'P5' or 'P6')\n");
    return false;
  }

  //on ne prend pas en compte les commentaires s'il y en a
  skip_comment(file, img);

  //on récupère les dimensions de l'image et on affiche une erreur s'il n'y a pas les deux dimensions
  if(fscanf(file, "%d %d\n", &img->data_header.width, &img->data_header.height) != 2){
    fprintf(stderr, "Impossible de lire la taille de l'image\n");
    return false;
  }

  //on ne prend pas en compte les commentaires s'il y en a. Il peut y avoir des commentaires ailleurs qu'entre le nombre magique et les dimensions, il peut y en avoir entre les dimensions et la valeur maximale d'un pixel
  skip_comment(file, img);

  //on récupère la valeur maximale qu'un pixel peut prendre
  if(fscanf(file, "%d\n", &img->data_header.max_value_pixel) != 1){
    fprintf(stderr, "Impossible de lire l'intensite maximal d'un pixel\n");
    return false;
  }

  //on ne prend pas en compte les commentaires s'il y en a. il peut y avoir des commentaires ailleurs qu'entre le nombre magique et les dimensions, il peut y en avoir entre la valeur maximale d'un pixel et les pixels
  skip_comment(file, img);

  //si la lecture de l'entête s'est bien passée, le pointeur (file*) pointe désormais au début de la partie avec l'ensemble des pixels codés en binaire
  return true;
}

boolean read_pixels_image(FILE* file, image* img){

  unsigned int nb_power = 0, value_pixel = 0;
  int nb_color = 1; //on suppose dans un premier temps que l'image est en noir et blanc

  //on va regarder sur combien d'octets s'écrit un pixel
  //on regarde sur combien de bits le nombre maximal que le pixel peut prendre s'écrit
  while(pow(2,nb_power) < img->data_header.max_value_pixel)
    nb_power++;
  //on convertit le nombre de bits en nombre d'octets
  int NB_OCTETS_A_LIRE = ceil(nb_power/8);

  //on regarde le nombre de couleurs par pixels d'image : par défaut 1 couleur (pour les pgm)
  if(img->data_header.magic_number[1] == '6'){ //si c'est une image ppm en couleur
    nb_color = 3;
  }

  //on récupère l'ensemble des pixels de l'image
  for( unsigned int k = 0; k < img->data_header.height * img->data_header.width * nb_color; k++) {
    //on lit les pixels écrit au format binaire, s'il en manque un on affiche une erreur
    if(fread(&value_pixel, NB_OCTETS_A_LIRE, 1, (FILE*)file) == 0){
      //si il manque un pixel
      fprintf(stderr, "Missing pixel(s)\n");
      return false;
    }
    //on affecte la valeur récupérée dans le tableau dynamique de la structure image
    img->pixels[k] = value_pixel;
    //on affiche à l'écran la valeur des pixels récupérée
    //printf("%d -", img->pixels[k]);
  }

  return true;
}

void read_text_console(char **url, char* printf_text){
  int idx = 0;
  char c;
  *url = (char*)malloc(1*sizeof(char));//on intitialise un tableau dynamique d'une case

  //on demande à l'utilisateur de saisir l'url de l'image
  printf(printf_text);
  //on récupère le premier caractère
  c = getchar();

  //on regarde si ce n'est pas un retour à la ligne
  if(c == '\n'){
    c = getchar();
  }

  //on répète tant qu'il reste des caractères à écrire
  while(c != '\n' && c != EOF){
    //on va ajouter un caractère dans la liste de char, on décale l'indice de la prochaine valeur dans le tableau
    idx++;

    //on agrandit le tableau et on y insère la valeur saisie par l'utilisateur
    *url = (char*)realloc(*url, idx*sizeof(char));
    (*url)[idx-1] = c;

    //on récupère le caractère suivant
    c = getchar();
  }

  //on rajoute le caractère de fin de chaîne : on agrandit le tableau dynamique d'une case et on rajoute '\0'
  *url = (char*)realloc(*url, (idx+1)*sizeof(char));
  (*url)[idx] = '\0';
  printf("L'URL saisie est : %s\n", *url);
}

FILE* open_file(char **url, char* mode, char* printf_text_url){
  FILE* file;
  /* ETAPE n°1 :  On demande à l'utilisateur l'url du fichier*/
  read_text_console(&(*url), printf_text_url);

  /* ETAPE n°2 :  On ouvre le fichier, si c'est possible*/
  file = fopen(*url,mode);

  return file;
}


boolean read_image(image* img, char **url, char *printf_text_url){
  //on initialise un booléen qui va permettre de savoir s'il y a eu un problème lors de la lecture de l'image
  boolean res;

  //Ouverture du fichier
  FILE* file = open_file(&(*url), "rb", printf_text_url);
  if(file == NULL){
    printf("Erreur lors de l'ouverture de l'image\n");
    return false;
  }

  //on demande à lire l'entête
  res = read_header_image(file,img);
  //s'il y a eu une erreur lors de la lecture de l'entête de l'image
  if(res == false){
    fprintf(stderr, "Invalid image header\n");
    return false;
  }

  //On initialise un tableau dynamique qui va stocker les valeurs des pixels de l'image
  //Nous avons décidé de stocker l'ensemble des pixels de l'image dans un tableau à une dimension
  if(img->data_header.magic_number[1] == '5'){ //s'il s'agit d'une image pgm (en nuance de gris)
    img->pixels = (unsigned int*) (malloc(img->data_header.width * img->data_header.height * sizeof(unsigned int)));
  }else{//s'il s'agit d'une image ppm en couleur (3 couleurs d'où le *3), seul ces deux formats peuvent arriver ici, sinon on serait déjà sorti de cette fonction via le res résultant de la lecture du header
    img->pixels = (unsigned int*) (malloc(img->data_header.width * img->data_header.height * sizeof(unsigned int) * 3));
  }
  //s'il y a eu un problème lors de l'allocation de la mémoire pour les pixels
  if(!img->pixels){
    fprintf(stderr, "Unable to allocate memory\n");
    return false;
  }

  //on cherche maintenant à récupérer l'ensemble des valeurs des pixels de l'image pour les stocker dans la structure image img
  res = read_pixels_image(file, img);
  //s'il y a eu un problème lors de la lecture des pixels de l'image
  if(res == false){
    fprintf(stderr, "Invalid pixels\n");
    return false;
  }

  //On ferme alors le fichier que l'on a ouvert puisque l'on a récupéré l'ensemble des valeurs qui sont contenues dans ce fichier
  fclose(file);

  //on return true car on a réussi à récupérer l'ensemble des données de l'image
  return res;
}
