/*!
*\file key.h
*
*\brief Bibliothèque pour pouvoir générer un ordre de parcours des pixels aléatoire
*
*\version 1.0
*\date 26/12/2020
*\author Antoine-Valentin CHARPENTIER & Bastien IVSAK
*/

#ifndef KEY_H
#define KEY_H

#include "utils.h"

/*!
*\brief Procédure qui permet de générer une liste de valeurs d'entiers à partir d'une chaîne de caractères saisie par l'utilisateur (clé) qui va contenir être les valeurs des srands
*\param key : Pointeur vers une clé, qui va contenir l'ensemble des informations générées par la clé
*\param srand_tab : Tableau d'entiers qui contient l'ensemble des valeurs de srand générées par la procédure à partir de la clé saisie par l'utilisateur
*/
void generate_srand_values(Key *key, unsigned int** srand_tab);
/*!
*\brief Procédure qui permet de réaliser un random pseudo-aléatoire d'un tableau [1,...,n] à partir de valeurs de srand générées
*\param key : Pointeur vers une clé, qui va contenir l'ensemble des informations générées par la clé
*\param srand_tab : Tableau d'entiers qui contient l'ensemble des valeurs de srand générées à partir de la clé saisie par l'utilisateur
*/
void shuffle_bits_order(Key *key, unsigned int* srand_tab);
/*!
*\brief Fonction qui génère un ordre de parcours des pixels à partir d'une clé saisie par l'utilisateur
*\return Une structure "Key" qui regroupe les informations nécessaires générées par la clé saisie par l'utilisateur
*/
Key generate_bits_order();

#endif
