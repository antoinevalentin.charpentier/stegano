# compilateur
CC := gcc
# options de compilation
CFLAGS := -Wall -Wextra

# règles de compilation
all : main

main : writing.o reading.o utils.o main.o encode.o decode.o key.o hamming.o matrix_vector.o
	$(CC) $(CFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

# options de compilation
run: clean main
	./main

memoire : main
	valgrind --track-origins=yes --leak-check=full ./main

clean:
	cls
	del *.o
	del *.exe